﻿CREATE TABLE [dbo].[DimClient]
(
	[ClientID] INT NOT NULL PRIMARY KEY, 
    [ClientName] VARCHAR(80) NOT NULL, 
    [BusinessSectorName] VARCHAR(30) NOT NULL, 
    [OrganisationTypeName] VARCHAR(30) NOT NULL, 
    [IsArchived] BIT NOT NULL, 
    [GovernmentBodyName] VARCHAR(50) NOT NULL 
)
