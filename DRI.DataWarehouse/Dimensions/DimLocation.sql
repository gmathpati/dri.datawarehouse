﻿CREATE TABLE [dbo].[DimLocation]
(
	[LocationID] INT NOT NULL PRIMARY KEY, 
    [LocationName] NVARCHAR(50) NULL, 
    [LocationType] VARCHAR(30) NOT NULL, 
    [LocationCode] VARCHAR(8) NOT NULL, 
    [Address1] VARCHAR(50) NOT NULL, 
    [Address2] VARCHAR(50) NOT NULL, 
    [Address3] VARCHAR(50) NOT NULL, 
    [Address4] VARCHAR(50) NOT NULL, 
    [Town] VARCHAR(50) NOT NULL, 
    [County] VARCHAR(30) NOT NULL, 
    [PostCode] VARCHAR(8) NOT NULL, 
    [Country] VARCHAR(50) NOT NULL, 
    [IsArchived] BIT NOT NULL, 
    [IsHeadOffice] BIT NOT NULL, 
    [DivisionName] VARCHAR(50) NOT NULL, 
    [DivisionType] VARCHAR(30) NOT NULL, 
    [ClientName] VARCHAR(80) NOT NULL, 
    [LastModifiedTimeStamp] DATETIME NOT NULL
)
