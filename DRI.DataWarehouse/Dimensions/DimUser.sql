﻿CREATE TABLE [dbo].[DimUser]
(
	[UserID] INT NOT NULL PRIMARY KEY, 
    [Username] VARCHAR(80) NOT NULL, 
    [FirstName] VARCHAR(30) NOT NULL, 
    [Surname] VARCHAR(30) NOT NULL, 
    [AccessEnabled] BIT NOT NULL, 
    [UserClientID] INT NOT NULL, 
    [UserClientName] VARCHAR(80) NOT NULL, 
    [UserType] VARCHAR(20) NOT NULL, 
    [TypeName] BIT NOT NULL
)
